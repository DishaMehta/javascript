const tests = [
    {
      "p": 73,
      "r": 98,
      "t": 11
    },
    {
      "p": 25,
      "r": 37,
      "t": 77
    },
    {
      "p": 34,
      "r": -44,
      "t": 69
    },
    {
      "p": 86,
      "r": 25,
      "t": 27
    },
    {
      "p": 27,
      "r": -38,
      "t": 11
    },
    {
      "p": 4,
      "r": 11,
      "t": 66
    }
   ]
var ans = [];
tests.forEach(function (testItem) {
    var x = testItem.p * testItem.r * testItem.t / 100;
    ans.push(x);
});
function removeNegative(ans){
    return ans.filter(el => el >= 0);
}
ans = removeNegative(ans);
//console.log(ans.filter(el => el >= 0));
// const finalans = ans.filter((value)=>{value>0});
for(let i=0;i<ans.length;i++){
    ans[i] = ans[i].toFixed(2);
}
console.log(ans);