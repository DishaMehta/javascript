var voters = [
    {name:'Bob' , age: 30, voted: true},
    {name:'Jake' , age: 32, voted: true},
    {name:'Kate' , age: 25, voted: false},
    {name:'Sam' , age: 20, voted: false},
    {name:'Phil' , age: 21, voted: true},
    {name:'Ed' , age:55, voted:true},
    {name:'Tami' , age: 54, voted:true},
    {name: 'Mary', age: 31, voted: false},
    {name: 'Becky', age: 43, voted: false},
    {name: 'Joey', age: 41, voted: true},
    {name: 'Jeff', age: 30, voted: true},
    {name: 'Zack', age: 19, voted: false}
 ];
 var objVoterResult = {
    youngVotes :'', 
    youth: '',
    midVotes: '',
    mids: '',
    oldVotes: '',
    olds: '',
 };
 var age1 = [];
 var age2 = [];
 var age3 = [];
 
     function CountVoter(arr){
          for(let i=0;i<arr.length;i++){
        let voter = arr[i];
        if(voter.age >= 18 && voter.age <= 25){
            age1.push(voter);
        }
        else if(voter.age >= 26 && voter.age <= 35){
            age2.push(voter);
        }
        else if(voter.age >= 36 && voter.age <= 55){
            age3.push(voter);
        }
     }
     objVoterResult.youth = age1.length;
     objVoterResult.mids = age2.length;
     objVoterResult.olds = age3.length;

 }
function ActuallyVotedCount(arr){
    let count = 0;
    for(let i=0;i<arr.length;i++){
        let trueVote = arr[i];
        if(trueVote.voted)
            count++;
    }
    return count;
}
 function voterResults(arr) {
    CountVoter(arr);
    let actuallyYouthVoted = ActuallyVotedCount(age1);
    objVoterResult.youngVotes = actuallyYouthVoted;
    
    let actuallyMidVoted = ActuallyVotedCount(age2);
    objVoterResult.midVotes = actuallyMidVoted;

    let actuallyOldVoted = ActuallyVotedCount(age3);
    objVoterResult.oldVotes = actuallyOldVoted;
    console.log(objVoterResult);
 }
 voterResults(voters);